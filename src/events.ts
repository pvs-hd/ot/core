/*
    The event bus contains two different systems
        1. Notifications:
            A component can notify others. This is a one way communication and does not/ cannot need an answer
        2. Request & Responses:
            A component can listen for requests on a channel and returns a response over the same

            Listening for requests is exclusive per channel. Only one component can do it at a time

        For consideration:
            - are 1 and 2 exclusive per channel? 
                Does a component listening for requests on a channel mean that notifications cannot be send 
*/

type CoreRequest = object
type CoreResponse = object | object[] | string

type RequestHandler = (request: CoreRequest, resolve: Function, reject: Function) => void

type ListenerLookup = { [index: string]: Function[] }

export class EventBus {
    notificationListener: { [index: string]: Function[] }
    requestListener: { [index: string]: RequestHandler }

    constructor() {
        this.notificationListener = {}
        this.requestListener = {}
    }

    public listenForNotification(channel: string, callback: Function) {
        if (this.notificationListener[channel]) {
            this.notificationListener[channel].push(callback)
        } else {
            this.notificationListener[channel] = [callback]
        }
    }

    public notify(channel: string, message: object) {
        if (this.notificationListener[channel]) {
            for (let subscriber of this.notificationListener[channel]) {
                subscriber()
            }
        }
    }

    public listenForRequest(channel: string, callback: RequestHandler): boolean {
        if (this.requestListener[channel]) {
            return false
        }

        this.requestListener[channel] = callback
        return true
    }

    public request(channel: string, request: CoreRequest): Promise<CoreResponse> {
        const handler = this.requestListener[channel]

        if (!handler) {
            return Promise.reject()
        }

        return new Promise((resolve, reject) => handler(request, resolve, reject))
    }
}
