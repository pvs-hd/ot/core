import { Knex, knex } from "knex"
import schemaInspector from "knex-schema-inspector"
import { SchemaInspector } from "knex-schema-inspector/dist/types/schema-inspector"

const config: Knex.Config = {
    client: "sqlite3",
    connection: {
        filename: "./data.db",
    },
    useNullAsDefault: true,
}

let db: Knex
let schema: SchemaInspector

export async function init(plugin: any) {
    db = knex(config)
    schema = schemaInspector(db)

    plugin
        .listenForRequest("database")
        .on("createTable", createTable)
        .on("deleteTable", deleteTable)
        .on("listTables", listTables)
        .on("insert", insert)
        .on("select", select)
        .on("update", update)
        .on("delete", deleteRow)
}

export async function close() {
    db.destroy()
}

async function createTable(request: any, resolve: any, reject: any) {
    const response = await db.schema.createTable(request.name, request.schema)
    resolve(response)
}

async function deleteTable(request: any, resolve: any, reject: any) {
    const response = await db.schema.dropTableIfExists(request.name)
    resolve(response)
}

async function listTables(request: any, resolve: any, reject: any) {
    const response = await schema.tables()
    resolve(response)
}

async function insert(request: any, resolve: any, reject: any) {
    const response = await db(request.table).insert(request.values)
    resolve(response)
}

async function select({ table, select = [], where = {} }: any, resolve: any, reject: any) {
    const response = await db.from(table).select(select).where(where)
    resolve(response)
}

async function update({ table, where = {}, update }: any, resolve: any, reject: any) {
    const response = await db(table).where(where).update(update)
    resolve(response)
}

async function deleteRow({ table, where = {} }: any, resolve: any, reject: any) {
    const response = await db(table).where(where).del()
    resolve(response)
}
