import { EventBus } from "../src/events"

let eventBus: EventBus
let callbackMock1: jest.Mock
let callbackMock2: jest.Mock

let promiseCallbackMock1: jest.Mock
let promiseCallbackMock2: jest.Mock

const channel = "channel"
const otherChannel = "otherChannel"

beforeEach(async () => {
    //needs to be reset every time
    // don't use any of the reset functions for this
    // it does not work https://github.com/facebook/jest/issues/7136
    callbackMock1 = jest.fn(() => {})
    callbackMock2 = jest.fn(() => {})

    promiseCallbackMock1 = jest.fn((request, resolve, reject) => resolve({}))
    promiseCallbackMock2 = jest.fn((request, resolve, reject) => resolve({}))

    eventBus = new EventBus() // set before each to not avoid state from previous tests
})

describe("notifying via the event bus", () => {
    test("subscribers are notified about events", () => {
        eventBus.listenForNotification(channel, callbackMock1)
        eventBus.notify(channel, {})

        expect(callbackMock1.mock.calls.length).toBe(1)
    })

    test("the callback function is only called when the event is triggered", () => {
        eventBus.listenForNotification(channel, callbackMock1)

        expect(callbackMock1.mock.calls.length).toBe(0)
    })

    test("subscribers are notified about mutliple events", () => {
        eventBus.listenForNotification(channel, callbackMock1)
        eventBus.notify(channel, {})
        eventBus.notify(channel, {})

        expect(callbackMock1.mock.calls.length).toBe(2)
    })

    test("multiple components can listen to the same channel", () => {
        eventBus.listenForNotification(channel, callbackMock1)

        eventBus.listenForNotification(channel, callbackMock2)

        eventBus.notify(channel, {})

        expect(callbackMock1.mock.calls.length).toBe(1)
        expect(callbackMock2.mock.calls.length).toBe(1)
    })

    test("multiple components can listen to different channels", () => {
        eventBus.listenForNotification(channel, callbackMock1)
        eventBus.listenForNotification(otherChannel, callbackMock2)

        eventBus.notify(channel, {})
        eventBus.notify(otherChannel, {})

        expect(callbackMock1.mock.calls.length).toBe(1)
        expect(callbackMock2.mock.calls.length).toBe(1)
    })
})

describe("requests and responds via the event bus", () => {
    test("components can listen on channels for requests", () => {
        eventBus.listenForRequest(channel, callbackMock1)

        eventBus.request(channel, {})

        expect(callbackMock1.mock.calls.length).toBe(1)
    })

    test("requests are answered by responses", async () => {
        const response = { message: "this is a response" }
        eventBus.listenForRequest(channel, (request, resolve, reject) => {
            resolve(response)
        })

        let recieved = await eventBus.request(channel, {
            message: "this is a request",
        })
        expect(recieved).toBe(response)
    })

    test("requests can be rejected", async () => {
        const error = { message: "this is an error" }
        eventBus.listenForRequest(channel, (request, resolve, reject) => {
            reject(error)
        })

        await eventBus.request(channel, { message: "this is a request" }).catch(recieved => {
            expect(recieved).toBe(error)
        })
    })

    test("only one component can listen for request on a channel", async () => {
        eventBus.listenForRequest(channel, promiseCallbackMock1)
        eventBus.listenForRequest(channel, promiseCallbackMock2)

        await eventBus.request(channel, {})

        expect(promiseCallbackMock1.mock.calls.length).toBe(1)
        expect(promiseCallbackMock2.mock.calls.length).toBe(0)
    })
})
